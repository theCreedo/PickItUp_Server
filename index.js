var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

// All the trash that has been picked
var picked_markers = {'list': []};

// All the trash that has not been picked
var unpicked_markers = {'list': []};

// Number of users currently using the app
var user_count = 0;

//
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

// Put Listener Functions
io.on('connection', function(socket){
  
  var user_num = ++user_count;

  console.log(currentTimeStamp() + ':\tUser '  + user_num +' connected');
  
  socket.on('disconnect', function(){
    
    console.log(currentTimeStamp() + ':\tUser '  + user_num +' disconnected');
  });

  // Display chat message for terminal
  socket.on('chat message', function(msg){
    
      io.emit('chat message', msg);
      var arr = msg.split('_');

      for(var i = 0; i< arr.length; i++) {

        var parse = arr[i].split(' ');

        console.log(currentTimeStamp() + ':\tparsing message - parse[0] = ' + parse[0]);
        // Takes in the first argument to determine whether points should be pushed or pulled.
        switch(parse[0]) {

          // push pin on the map
          case 'push':
            console.log(currentTimeStamp() + ':\tReceived TERMINAL PUSH Pin Request from User ' + user_num);
            
            // Makes sure number of arguments is 3
            if(parse.length == 3) {
              
              io.emit('chat message', 'push_received');
              console.log(currentTimeStamp() + ':\tPush Received lati: ' + parse[1] + 'long: ' + parse[2]);

              var pin = {latitude: parse[1], longitude: parse[2]};
              
              if (unpicked_markers.list == null) {
                
                console.log(currentTimeStamp() + ':\tInstantiating list...');
                unpicked_markers.list = [];
                console.log(currentTimeStamp() + ':\tInstantiated');
              }

              console.log(currentTimeStamp() + ':\tPushing marker...')
              unpicked_markers.list.push(pin);
              console.log(currentTimeStamp() + ':\tSuccessfully Pushed');
            } else {
              console.log(currentTimeStamp() + ':\tError: Number of arguments != 3');
            }
            break;

          // pull pin off the map
          case 'pull':
            console.log(currentTimeStamp() + ':\tReceived TERMINAL PULL Pin Request from User ' + user_num);
            
            // Makes sure number of arguments is 3
            if(parse.length == 3) {
              
              io.emit('chat message', 'pull_received');
              console.log(currentTimeStamp() + ':\tPull Received lati: ' + parse[1] + 'long: ' + parse[2]);

              var lati = parse[1];
              var longi = parse[2];

              if (picked_markers.list == null) {
                
                console.log(currentTimeStamp() + ':\tInstantiating list...');
                picked_markers.list = [];
                console.log(currentTimeStamp() + ':\tInstantiated');
              }
              
              var found = false;

              for(var i = 0;  i < picked_markers.list.length; ++i){
                
                if(picked_markers.list[i].latitude == lati && picked_markers.list[i].longitude == longi){
                  
                  found = true;

                  console.log(currentTimeStamp() + ':\tRemoving marker...')
                  picked_markers.list.splice(i, 1);
                  console.log(currentTimeStamp() + ':\tSuccessfully Removed');
                  break;
                }
              }

              if(!found) {
                var pin = {latitude: lati, longitude: longi};
                picked_markers.list.push(pin);
              }

            } else {
              console.log(currentTimeStamp() + ':\tPullFailedError: Number of arguments != 4');
            }
            break;
          default:
            break;
        }
    }
  });

  // Put pin on map
  socket.on('push_pin', function(lati, longi){
    
    console.log(currentTimeStamp() + ':\tReceived PUSH Pin Request from User ' + user_num);

    var pin = {latitude: lati, longitude: longi};
 
    unpicked_markers.list.push(pin);
    console.log(currentTimeStamp() + ':\tSuccessfully pushed latitude: ' + lati + ' and longitude: ' + longi);

    io.emit('matrix', unpicked_markers);
  });

  // Delete pin from map
  socket.on('pull_pin', function(lati, longi){

    console.log(currentTimeStamp() + ':\tReceived PULL Pin Request from User ' + user_num);

    var found = false;

    // Go through all unpicked_markers to find a point
    for(var i = 0;  i < unpicked_markers.list.length; ++i){

      // Check if latitude and longitude are located in the list
      if(unpicked_markers.list[i].latitude == lati && unpicked_markers.list[i].longitude == longi){
        found = true;
        var removed_marker = unpicked_markers.list.splice(i, 1); // remove marker
        
        console.log(currentTimeStamp() + ':\tSuccessfully pulled pin at latitude: ' + lati + ' and longitude: ' + longi);
        picked_markers.list.push(removed_marker[0]); // Pushing removed marker to picked_marker list
        console.log(currentTimeStamp() + ':\tAdded pin from unpicked_markers to picked_markers');

        break;
    }

    // Self picked trash
    if(!found){
      var pin = {latitude: lati, longitude: longi};
      picked_markers.list.push(pin);
    }
  }

  });

  // Poll data to polling app
  socket.on('poll', function(callback){
      console.log(currentTimeStamp() + ':\tReceived Poll Request from User ' + user_num);
      
      console.log(currentTimeStamp() + ':\tPolling data...');
      callback(unpicked_markers.list, picked_markers.list); // Sending lists to app
      console.log(currentTimeStamp() + ':\tSent poll data');
  });
});

// Heart of the server code (this function is called every interval)
// With current implementation though, do we need this???
setInterval(function(){
    console.log(currentTimeStamp() + ':\tEmitting markers...');
    if(picked_markers.list.length > 0) {

      // Printing out all the PINNED data
      for (var key in picked_markers) {
        var data = picked_markers[key];

        for (var i = 0; i < data.length; ++i) {
          console.log(currentTimeStamp() + ':\tEmitting picked_marker ' + i + ': ' + data[i].latitude + ',' + data[i].longitude);
        }
      }
    }

    if(unpicked_markers.list.length > 0) {

      // Printing out all the PINNED data
      for (var key in unpicked_markers) {
        var data = unpicked_markers[key];
        
        for (var i = 0; i < data.length; ++i) {
          console.log(currentTimeStamp() + ':\tEmitting unpicked_marker ' + i + ': ' + data[i].latitude + ',' + data[i].longitude);
        }
      }
    }

}, 1000);

http.listen(3000, function(){
  console.log(currentTimeStamp() + ':\tlistening on *:3000');
});

function currentTimeStamp() {
  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  return date + ' ' + time;
}
